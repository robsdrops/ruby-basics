class Alphabet
  include Enumerable

  @@alphabet = 'a'..'z'
  def each
    @@alphabet.each { |a| yield a }
  end
end

alpha = Alphabet.new
alpha.each {|a| puts a}
alpha.collect {|a| puts a + ' - element of alphabet!'}
alpha.detect {|a| a == 'w'}
puts alpha.max