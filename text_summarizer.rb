text = File.readlines('text.txt').join
sentences = text.gsub(/\s+/,' ').split(/\.|!|\?/)
sentences_ordered = sentences.sort_by { |sentence| sentence.length }
one_third = sentences_ordered.length / 3
ideal_sentences = sentences_ordered.slice(one_third, one_third + 1)
ideal_sentences = ideal_sentences.select { |sentence| sentence =~ /is|are|/ }
ideal_sentences.collect! { |ideal_sentence| ideal_sentence.strip }
puts ideal_sentences