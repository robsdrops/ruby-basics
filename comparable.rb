require 'profile'

class Car
  attr_accessor :power

  include Comparable

  def initialize(model ,power)
    @model = model
    @power = power
  end

  def <=>(other)
    @power <=> other.power
  end
end

small_car = Car.new('Hatchback', 100)
huge_car = Car.new('Combi', 300)
puts small_car >= huge_car