require 'benchmark'
Benchmark.bmbm do |x|
  x.report('10000: ') { 10000.times { '.' } }
end

Benchmark.bmbm do |x|
  x.report('100000: ') { 100000.times { '.' } }
end